import { AsyncStorageKeys } from '../utility/Constants';
import AsyncStorage from "@react-native-community/async-storage";
import axios from '../api/apiCall'
import createDataContext from './createDataContext';

const userRegisterReducer = (state, action) => {
    console.log('action_received', action);
    switch (action.type) {
        case 'register_user':
            return action.payload;
        default:
            return state;
    }
}

const submitUserRegisterDetail = dispatch => {
    return async (userData, callback) => {
        const response = await axios.post('/users/register', userData)
            .then(async function (response) {
                try {
                    const responseData = response.data.data
                    dispatch({ type: 'register_user', payload: response.data });
                    if (callback) {
                        callback(response)
                    }
                } catch (error) {
                    console.log("Catch at AppContext =>", error)
                }
            })
            .catch(function (error) {
                if (error.response) {
                    callback(error.response)
                } else if (error.request) {
                    callback(error.request)
                } else {
                    callback({ response: "" })
                }
            });
    };
};

export const { Context, ProviderFun } =
    createDataContext(
        userRegisterReducer,
        { submitUserRegisterDetail },
        {});