import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import ColorsHelper from '../utility/ColorsHelper';

class EditText extends Component {
    state = {
        isValid: true,
        showPassword: true,
    };

    componentDidMount() {
        // AsyncStorage.setItem('name', 'Demo Name');
    }

    getName = () => {
        // AsyncStorage.getItem('name').then((value) => 
        // {
        //     console.log("Data Fetched => ", value);
        // })
    }

    handleValidation(value) {
        const { regexPattern } = this.props;
        if (!regexPattern) return true;
        // string pattern, one validation rule
        if (typeof regexPattern === 'string') {
            const condition = new RegExp(regexPattern);
            return condition.test(value);
        }
        // array patterns, multiple validation rules
        //   if (typeof pattern === 'object') {
        //     const conditions = pattern.map(rule => new RegExp(rule, 'g'));
        //     return conditions.map(condition => condition.test(value));
        //   }
    }
    onChange(value) {
        // const { onChangeText, onValidation } = this.props;
        const isValid = this.handleValidation(value)
        // console.log("State Valie isValid value => ",value, this.props.regexPattern,  isValid);
        this.setState({ isValid });
        // onValidation && onValidation(isValid);
        // onChangeText && onChangeText(value);
    }
    render() {
        // console.log("State Valie isValid", this.state.isValid);

        const {
            placeHolder,
            dataToInput,
            textUpdate,
            onTextSubmit,
            isSecureInput,
            pattern,
            onChangeText,
            children,
            style,
            regexPattern,
            keyboardType,
            ...props
        } = this.props;
        return (
            <View style={styles.searchViewBack}>
                <TextInput
                    autoCapitalize='none'
                    autoCorrect={false}
                    placeholder={placeHolder}
                    style={styles.inputStyle}
                    secureTextEntry={this.state.showPassword && isSecureInput}
                    // value={dataToInput}
                    onChangeText={newData => {
                        textUpdate(newData)
                        // this.onChange(newData)
                    }}
                    keyboardType={keyboardType}
                    {...props}
                >
                    {children}

                </TextInput>
            </View>

        );
    }
}

EditText.defaultProps = {
    initialValues: {
        isSecureInput: false,
        placeHolder: '',
        regexPattern: '',
        keyboardType:'default',
    }
}

const styles = StyleSheet.create({
    searchViewBack: {
        backgroundColor: ColorsHelper.white,
        borderRadius: 8,
        flexDirection: "column",
        marginHorizontal: 32,
        marginVertical: 8,
        marginBottom: 4
    },
    inputStyle: {
        borderColor: ColorsHelper.editTextBorderColor,
        borderWidth: 0.5,
        borderRadius: 4,
        paddingHorizontal: 8,
        paddingLeft: 4,
        height: 48,
        paddingRight: 28,
    },
    errorHintStyle: {
        alignSelf: 'flex-start',
        fontSize: 12,
        color: ColorsHelper.textInputError
    }
});

export default EditText;