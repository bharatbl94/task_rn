import { View, StyleSheet, Text, Image } from 'react-native';
import React from 'react';
import ColorsHelper from '../utility/ColorsHelper';
const UserCard = ({ userObj }) => {
    return (
        <View style={styles.container}>
            <View
                style={
                    styles.cardContainerStyle
                }>
                <View style={
                    styles.profileIconView
                }>
                    {userObj.profile_pic
                        ?
                        <Image style={
                            styles.profileImage
                        } source={{ uri: userObj.profile_pic }} /> :
                        <Text style ={styles.textTitleStyle}>PC</Text>
                    }
                </View>
                <Text style={styles.textTitleStyle}>{userObj.username}</Text>
                <Text style={styles.textTitleStyle}>{userObj.email}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 8,
        alignItems: 'center',
        flexDirection: 'column',
        flex: 1,
    }, profileIconView: {
        padding: 8,
        alignItems: 'center',
        width: 48,
        alignSelf:'center',
        height: 48,
        borderRadius: 4,
        justifyContent: 'center',
    },
    profileImage: {
        alignItems: 'center',
        width: 48,
        height: 48,
    },
    cardContainerStyle: {
        flexDirection: 'column',
        padding: 8,
        marginHorizontal: 12,
        flex: 1,
        width: '100%',
        backgroundColor: ColorsHelper.white,
        justifyContent: 'space-between',
        shadowColor: ColorsHelper.grey30,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 20,
        elevation: 4,
        borderRadius: 4,
    },
    textTitleStyle: {
        // backgroundColor: ColorsHelper.white,
        margin: 4,
        fontSize: 16,
        alignSelf: "center",
        justifyContent: "center",
        fontStyle: "normal",
        color: ColorsHelper.grey70,
    },
    textDataStyle: {
        // backgroundColor: ColorsHelper.white,
        fontSize: 14,
        alignSelf: "center",
        justifyContent: "center",
        fontStyle: "normal",
        color: ColorsHelper.grey55,
    }
});

export default UserCard;