import { createStackNavigator } from "react-navigation-stack";
import React from 'react';
import UserRegistrationScreen from "../screens/UserRegistrationScreen";
import UserList from "../screens/UserList";
export const createRootNavigator = (signedIn = false) => {
    return createStackNavigator(
        {
            UserList: UserList,
            UserRegistrationScreen: UserRegistrationScreen
        },
        {
            initialRouteName: signedIn ? "UserList" : "UserRegistrationScreen",
            defaultNavigationOptions: {
                headerShown: true,
            }
        }
    );
};