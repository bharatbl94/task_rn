import { AsyncStorageKeys } from '../utility/Constants';
import AsyncStorage from '@react-native-community/async-storage';

// export const onSignIn = () => AsyncStorage.setItem(USER_KEY, "true");

// export const onSignOut = () => AsyncStorage.removeItem(USER_KEY);

export const isSignedIn = () => {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem(AsyncStorageKeys.USER_LOGGED_IN)
      .then(res => {
        if (res !== null) {
          resolve(true);
        } else {
          resolve(false);
        }
      })
      .catch(err => reject(err));
  });
};