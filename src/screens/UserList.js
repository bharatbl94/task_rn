import React, { Component } from 'react';
import {
  View, Text, StyleSheet, ActivityIndicator,
  FlatList, SafeAreaView, RefreshControl
} from 'react-native';
import { getUserListApi } from '../api/apiCallingModule';
import UserCard from '../components/UserCard';
import ColorsHelper from '../utility/ColorsHelper';

class UserList extends Component {
  pageIndex = 0;
  totalUserList = 0; totalUserCount = 0; currentPage = 1; isDataLoading = false;
  constructor() {
    super();
    this.state = {
      userList: [],
      lastPage: 0,
      currentPage: 0,
      loading: false,
      isRefreshing: false,
      firstTimeTimeLoad: true,
      //Index of the offset to load from web API
    }
  }
  componentDidMount() {
    this.getUserList()
    // AsyncStorage.setItem('name', 'Demo Name');
  }

  onRefreshData = async () => {
    this.setState({ isRefreshing: true })
    this.pageIndex = 1;
    getUserListApi(this.pageIndex, (response, isSuccess) => {
      this.setState({ isRefreshing: false })
      if (isSuccess && response.status === 200) {
        const responseData = response.data.data
        const responseList = responseData.data
        this.currentPage = responseData.current_page
        this.setState({ notificationList: responseList })
      }
    })
  };

  getUserList = async () => {
    this.setState({ loading: true, })
    this.isDataLoading = true
    getUserListApi(++this.pageIndex, (response, isSuccess) => {
      this.setState({ loading: false, firstTimeTimeLoad: false })
      this.isDataLoading = false
      if (isSuccess && response.statusCode === 200) {
        const responseData = response.body.data
        const responseList = responseData.users
        const pagination = responseData.pagination
        const userList = this.state.userList
        const userConcatList = userList.concat(responseList)
        this.totalUserCount = pagination.total
        this.userListCount = userConcatList.length
        const lastPage = pagination.lastPage
        this.currentPage = +pagination.page
        this.setState({ userList: userConcatList, lastPage: lastPage, currentPage: this.currentPage })
      }
    })
  };

  onViewableItemsChange = ({ viewableItems }) => {
    try {
      const lastVisibleIndex = viewableItems.length - 1
      const lastVisibleObj = viewableItems[lastVisibleIndex]
      if (lastVisibleObj.index >= this.userListCount - 2 && this.totalUserCount > this.userListCount && !this.isDataLoading) {
        this.isDataLoading = true
        this.getUserList()
      }
    } catch (error) {
      console.log(error)
    }
  }

  renderFooter = () => {
    return (
      //Footer View with Load More button
      this.state.loading && this.userListCount != 0 ? (
        <View style={this.styles.footer}>
          <Text style={this.styles.loadText}>Loading</Text>
          <ActivityIndicator color="grey" style={{ marginLeft: 2 }} />
        </View>
      ) : null
    );
  }

  renderSeparator = () => (
    <View
      style={{
        backgroundColor: 'lightgrey',
        height: 0.5,
        width: '100%'
      }}
    />
  )

  render() {
    const {
      ...props
    } = this.props;

    return (
      <>
        {this.state.firstTimeTimeLoad ?
          <View style={this.styles.containerLoader}>
            <ActivityIndicator size="large" animating={this.state.firstTimeTimeLoad} color={ColorsHelper.appPrimaryColorBlue} />
          </View> :
          <SafeAreaView style={this.styles.windowStyle}
          >
            <View style={{ flex: 1 }}>
              <FlatList
                data={this.state.userList}
                contentContainerStyle={[{ flexGrow: 1 }, this.state.userList.length ? null : { justifyContent: 'center' }]}
                disableVirtualization={false}
                keyExtractor={(userObj) => userObj.id}
                ItemSeparatorComponent={this.renderSeparator}
                renderItem={({ item }) => {
                  return (
                    <UserCard
                      userObj={item}
                    />
                  )
                }}
                bounces={true}
                ListEmptyComponent={
                  <Text style={{
                    alignSelf: 'center',
                    color: ColorsHelper.appPrimaryColorBlue,
                    fontSize: 18,
                  }}>No Data Found</Text>
                }
                // refreshControl={
                //   <RefreshControl
                //     refreshing={this.state.isRefreshing}
                //     onRefresh={
                //       this.onRefreshData.bind(this)
                //     }
                //   />
                // }
                onViewableItemsChanged={
                  this.onViewableItemsChange
                }
                extraData={this.state}
                viewabilityConfig={{ itemVisiblePercentThreshold: 20 }}
                ListFooterComponent={this.renderFooter}
              >
              </FlatList>

            </View>
          </SafeAreaView>
        }
      </>
    );
  }

  styles = StyleSheet.create({
    windowStyle: {
      backgroundColor: ColorsHelper.white,
      flex: 1
    },
    containerLoader: {
      flex: 1,
      justifyContent: 'center',
      position: 'absolute',
      alignSelf: 'center',
      flexDirection: 'column',
      // justifyContent: 'space-around',
      padding: 10,
      marginBottom: 20,
      top: 0, bottom: 0, left: 0, right: 0, zIndex: 0
    },
    footer: {
      height: 24,
      // flex:1,
      marginBottom: 18,
      justifyContent: 'center',
      flexDirection: 'row',
    },
    loadText: {
      color: 'black',
      fontSize: 14,
      marginHorizontal: 4,
      textAlign: 'auto',
      alignSelf: "center"
    },
  })

}

export default UserList;