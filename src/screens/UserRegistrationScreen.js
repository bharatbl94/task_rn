import React, { useState, useContext } from 'react';
import { View, Text, StyleSheet, Image, TextInput, ActivityIndicator, TouchableOpacity } from 'react-native';
import EditText from '../components/EditText';
import { Context } from '../AppContext/AppContext';
import { AsyncStorageKeys } from '../utility/Constants';
import { createAlertWithSingleButton } from '../utility/alertComponent';
import AsyncStorage from '@react-native-community/async-storage';
import ColorsHelper from '../utility/ColorsHelper';
import userRegisterResult from '../api/userRegisterResult';
import { postCreateUserApi } from '../api/apiCallingModule';


const UserRegistrationScreen = ({ navigation }) => {
    const [doShowLoader, updateLoaderStatus] = useState(false);
    const [doUserRegister, results, errorMessage] = userRegisterResult();
    const [userEmail, emailUpdate] = useState('');
    const [userPassword, passwordUpdate] = useState('');
    const [userUsername, usernameUpdate] = useState('');
    const { state, getLoginUserData } = useContext(Context);
    return (
        <View style={{ flex: 1, flexDirection: 'column', backgroundColor: ColorsHelper.white, paddingTop:48 }}>
            <Text style={styles.textBoxTitleStyle}>Username</Text>
            <EditText placeHolder="" textUpdate={usernameUpdate} />
            <Text style={styles.textBoxTitleStyle}>Email</Text>
            <EditText placeHolder="" textUpdate={emailUpdate} keyboardType="email-address"
                regexPattern='^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,12})$' />
            <Text style={styles.textBoxTitleStyle}>Password</Text>
            <EditText placeHolder=""
                textUpdate={passwordUpdate} isSecureInput={true} />
            <TouchableOpacity title="Register"
                style={styles.buttonStyle}
                onPress={() => {
                    //To check whether all entries has been input
                    if (!userEmail && !userPassword && !userUsername) {
                        createAlertWithSingleButton("Alert!", "Please input all mandatory fields.");
                        return
                    }
                    const registerBody = { 'email': userEmail, 'password': userPassword, 'username': userUsername }
                    postCreateUserApi(registerBody, async (response) => {
                        updateLoaderStatus(false)
                        try {
                            if (response.statusCode === 201) {
                                AsyncStorage.setItem(AsyncStorageKeys.USER_LOGGED_IN, JSON.stringify(true));
                                AsyncStorage.setItem(AsyncStorageKeys.USER_TOKEN, response.body.data.token.token);
                                navigation.replace('UserList');
                            }
                            //User Not Registered
                            else if (response.statusCode === 422) {
                                createAlertWithSingleButton(
                                    "Error", response.body.meta.message);
                            }
                            else {
                                createAlertWithSingleButton(
                                    "Error", "Something went wrong");
                            }
                        } catch (err) {
                            console.log("At catch", err);
                            createAlertWithSingleButton(
                                "Error", "Something went wrong");
                        }
                    })
                }
                }>
                <Text style={styles.buttonTextStyle}>Register</Text>

            </TouchableOpacity>
            {doShowLoader ? <View style={[styles.containerLoader]}>
                <ActivityIndicator size="large" animating={doShowLoader} color={ColorsHelper.appPrimaryColorBlue} />
            </View> : null}

        </View>
    )
}

const styles = StyleSheet.create({
    textBoxTitleStyle: {
        marginTop: 18,
        marginLeft: 32,
        fontSize: 18
    }, titleStyle: {
        marginHorizontal: 32,
        fontSize: 28,
        alignSelf: "center"
    }, buttonStyle: {
        marginTop: 24,
        marginHorizontal: 32,
        backgroundColor: ColorsHelper.appPrimaryColorBlue,
        height: 44,
        justifyContent: 'center',
        borderRadius: 4,
    }, buttonTextStyle: {
        color: ColorsHelper.white,
        alignSelf: 'center',
        fontSize: 18,
        fontWeight: "600"
    },
    containerLoader: {
        flex: 1,
        justifyContent: 'center',
        position: 'absolute',
        alignSelf: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        padding: 10,
        marginBottom: 20,
        top: 0, bottom: 0, left: 0, right: 0, zIndex: 0
    },
});

export default UserRegistrationScreen