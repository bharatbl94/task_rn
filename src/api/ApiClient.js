/** @format */
import AsyncStorage from "@react-native-community/async-storage";
import axios from "axios";
import { AsyncStorageKeys } from "../utility/Constants";
class ApiClient {
  constructor(options) {
    this.baseUrl = options.baseUrl;
  }

  post(endpoint, params = null, headers = null) {
    return this.requestHttp("POST", this.baseUrl + endpoint, params, headers);
  }

  async get(endpoint, queryParams = null, headers = null) {
    return this.requestHttp("GET", this.baseUrl + endpoint, null, queryParams, headers);
  }

  put(endpoint, params, headers = null) {
    return this.requestHttp("PUT", this.baseUrl + endpoint, params, headers);
  }

  patch(endpoint, params, headers = null) {
    return this.requestHttp("PATCH", this.baseUrl + endpoint, params, headers);
  }

  delete(endpoint, params, headers = null) {
    return this.requestHttp("DELETE", this.baseUrl + endpoint, params, headers);
  }

  async requestHttpNoAuth(method, url, data, headers) {
    return new Promise((resolve, reject) => {
      const options = {
        method,
        headers: {
          "Content-Type": "application/json",
        },
      };
      if (data) {
        options.body = JSON.stringify(data);
      }
      if (headers) {
        Object.assign(options.headers, headers);
      }
      console.log("****At Request*****", "url=>>>\n" + url + "options=>>>\n" + JSON.stringify(options))
      fetch(url, options)
        .then((response) => {
          response
            .json()
            .then((body) => {
              console.log("\n\n*****At Response***** URL=>>>", url +
                "\n\noptions=>>>\n" + JSON.stringify(options) + "response=>>>\n" + JSON.stringify(response) + "\nResponse Body=>>>\n" + JSON.stringify(body))
              resolve({ statusCode: response.status, body });
            })
            .catch((error) => {
              console.error(`${url}`, error)
              reject(error);
            });
        })
        .catch((error) => {
          console.error(error)
          reject(error);
        });
    });
  }


  async requestHttp(method, url, data, queryParams, headers) {
    const authToken = await AsyncStorage.getItem(AsyncStorageKeys.USER_TOKEN);
    return new Promise((resolve, reject) => {
      const options = {
        method,
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${authToken}`,
        },
      };
      if (data) {
        options.body = JSON.stringify(data);
      }
      if (queryParams) {
        const params = new URLSearchParams(queryParams);
        url = `${url}?${params}`
      }
      if (headers) {
        Object.assign(options.headers, headers);
      }
      console.log("\n\n*****At Request*****\n\n", "URL=>>>\n" + url + "\noptions=>>>\n" + JSON.stringify(options))
      fetch(url, options)
        .then((response) => {
          response
            .json()
            .then((body) => {
              console.log("\n\n*****At Response*****\n\nURL=>>>\n", url +
                "\n\noptions=>>>\n" + JSON.stringify(options) + "response=>>>\n" + JSON.stringify(response) + "\nResponse Body=>>>\n" + JSON.stringify(body))
              resolve({ statusCode: response.status, body });
            })
            .catch((error) => {
              console.error(error)
              reject(error);
            });
        })
        .catch((error) => {
          console.error(error)
          reject(error);
        });
    });
  }
}


export default ApiClient;
