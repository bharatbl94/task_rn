import ApiClient from './ApiClient'


const ApiFromClient = new ApiClient({
    baseUrl: 'http://68.183.48.101:3333',
});


export const postCreateUserApi = async (postData, callback) => {
    try {
        ApiFromClient.requestHttpNoAuth("POST",`${ApiFromClient.baseUrl}/users/register`, postData).then(function (response) {
            if (response.statusCode == 201) {
                callback(response, true)
            } else {
                callback(response, false)
            }
        }).catch(function (error) {
            callback(error, false)
        })
    } catch (err) {
        callback({}, false)
    }
};

export const getUserListApi = async (page, callback) => {
    try {
        ApiFromClient.get(`/users/list`,{page: page}).then(function (response) {
            if (response.statusCode == 200) {
                callback(response, true)
            } else {
                callback(response, false)
            }
        }).catch(function (error) {
            callback(error, false)
        })
    } catch (err) {
        callback({}, false)
    }
};