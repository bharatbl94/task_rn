import axios from 'axios';

export default axios.create({
    baseURL: 'http://68.183.48.101:3333',
    headers: {
        accept: 'application/json',
    }
});