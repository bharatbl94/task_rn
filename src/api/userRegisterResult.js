import { useState, useEffect } from 'react'
import apiHelper from '../api/apiCall'

export default () => {
    const [results, setResults] = useState([]);
    const [errorMessage, setError] = useState('');

    const doUserRegister = async (inputDataLogin) => {
        try {
            console.log(inputDataLogin, apiHelper )
            const response = await apiHelper.post('/users/register', 
               inputDataLogin
            ).then(function (response) {
                console.log("At Axios",JSON.stringify(response.status));
              })
              .catch(function (error) {
                console.log("At Axios error", error);
              });

            console.log("response received", response)
            setError('')
        } catch (err) {
            console.log(err)
            setError('__Something went wrong__')
        }
    };
  
    return [doUserRegister, results, errorMessage];
};
