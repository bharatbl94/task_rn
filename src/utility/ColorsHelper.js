const lightGrey = '#d4d4d4';

const ColorsHelper = {
    editTextBorderColor: lightGrey,
    separatorColor: lightGrey,
    white: 'white',
    black: 'black',
    appPrimaryColorBlue: '#394eb4',
    errorBackground: 'red',
    errorText: '#fff',
    textInputError: '#f56c6c',
    greyColor: '#979797',
    greyLight: '#E5E5E5',
    grey10: '#D6D6D6',
    grey20: '#C1C1C1',
    grey30: '#ACACAC',
    grey40: '#9C9C9C',
    grey50: '#838383',
    grey52: '#626262',
    grey55: '#525252',
    grey60: '#323232',
    grey70: '#4F4F4F',
    grey80: '#414141',
};

export default ColorsHelper;