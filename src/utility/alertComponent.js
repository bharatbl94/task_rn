import React, { useState } from "react";
import { Alert } from "react-native";

const createTwoButtonAlert = (alertTitle, alertMessage, callback, positiveText = "Ok", negativeText = "Cancel") => {
    Alert.alert(
        alertTitle,
        alertMessage,
        [  
            {
                text: negativeText,
                onPress: () => {
                    if (callback) {
                        callback(false)
                    }
                },
                style: "cancel"
            },
            {
                text: positiveText, onPress: () => {
                    if (callback) {
                        callback(true)
                    }
                }
            },
         
        ],
        { cancelable: false }
    );
};

const createAlertWithSingleButton = (alertTitle, alertMessage, callback, buttonText = "OK") => {
    Alert.alert(
        alertTitle,
        alertMessage,
        [
            {
                text: buttonText, onPress: () => {
                    console.log("Ok Pressed")
                    if (callback) {
                        callback(true)
                    }
                }
            }
        ],
        { cancelable: false }
    );
};

export { createTwoButtonAlert, createAlertWithSingleButton  };
