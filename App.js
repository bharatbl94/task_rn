import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createAppContainer } from "react-navigation";
import { createRootNavigator } from "./src/main/route";
import { isSignedIn } from "./src/main/auth";
import { ProviderFun } from "./src/AppContext/AppContext"
import NavigationService from './src/service/NavigationService';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      signedIn: false,
      checkedSignIn: false,
    };
  }
  componentDidMount() {
    isSignedIn()
      .then(res => this.setState({ signedIn: res, checkedSignIn: true }))
      .catch(err => this.setState({ signedIn: false, checkedSignIn: true }));
  }

  render() {
    const { checkedSignIn, signedIn, appIsReady } = this.state;
    const Layout = createRootNavigator(signedIn);
    const App = createAppContainer(Layout)
    return (
      <ProviderFun>
        <App ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}/>
      </ProviderFun>)
  };
}

